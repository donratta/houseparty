var mongoose = require('../db').mongoose;
var Schema = require('../db').Schema;

var playlistSongSchema = new Schema ({
    playlist : {type:Schema.ObjectId, ref:'HouseParty'},
    song:{type:Schema.ObjectId, ref:'Song'},
    votes:{type:Number,default:0},
    timeAdded:{type:Date,default:new Date()},
    personAdding:String
});


var PlaylistSong = mongoose.model('PlaylistSong', playlistSongSchema);

module.exports = PlaylistSong;