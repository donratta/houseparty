var mongoose = require('../db').mongoose;
var Schema = require('../db').Schema;

var songSchema = new Schema ({
    name : String,
    artist:String,
    artworkUrl:String,
    dateCreated:{type:Date,default:new Date()},
    genre:{type:String,default:"Unknown"},
    url:String
})


var Song = mongoose.model('Song', songSchema);

var songs = require('./songFilesDev');

//console.log(songs)
 //console.log(songs[0])
//Song.create(songs,function(err){

    //if(err){
      //  throw(err);
    //}
//})

module.exports = Song