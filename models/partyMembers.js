var mongoose = require('../db').mongoose;
var Schema = require('../db').Schema;

var partyMembersSchema = new Schema ({
    playlist : {type:Schema.ObjectId, ref:'HouseParty'},
    nickName:String,
    timeJoined:{type:Date,default:new Date()},
    admin:{type:Boolean, default:false}
});


var PartyMember = mongoose.model('PartyMember', partyMembersSchema);

module.exports = PartyMember;