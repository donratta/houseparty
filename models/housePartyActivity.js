var mongoose = require('../db').mongoose;
var Schema = require('../db').Schema;

var housePartyActivitySchema = new Schema ({
    playlist : {type:Schema.ObjectId, ref:'HouseParty'},
    activity:String,
    error:Boolean,
    dateCreated:{type:Date,default:new Date()},
    user:{type:Schema.ObjectId, ref:'PartyMember'}
})


var HousePartyActivity = mongoose.model('HousePartyActivity', housePartyActivitySchema);



//console.log(songs)
//console.log(songs[0])
//Song.create(songs,function(err){

//if(err){
//  throw(err);
//}
//})

module.exports = HousePartyActivity