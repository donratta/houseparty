angular.module('HouseParty.services.socket',[])


.factory('SocketManager',function($rootScope,SoundManager){
        var socket = io.connect();
        //add socket listeneres
        socket.on( 'getNewSong', function( dataPackage ) {
            console.log(dataPackage);
            //compare the ID of the playlist
            if(dataPackage.data.partyId == SoundManager.getCurrentPlaylist()._id){
                toastr.success(dataPackage.info);
                //set the new songlist

                SoundManager.setPlaylistSongs(dataPackage.newSongList);
                console.log(SoundManager.getPlaylistSongs())
                $rootScope.$broadcast('newPartyActivity')
                if(SoundManager.getPlaylistSongs().length==1){
                    SoundManager.initializePlayer();
                }
                else{
                    SoundManager.pushNewTrackToStack();
                }

            }
            return;
        })
        socket.on('newPartyMember',function(data){
            //$rootScope.$broadcast('partyMemberChange');
             toastr.success(data)

        })
        socket.on('newMem',function(data){
            toastr.success(data)
            $rootScope.$broadcast('partyMemberChange');
        })
        return {
            addNewSong:function(songId,userId,partyId){
                socket.emit('addNewSong',{songId:songId,userId:userId,partyId:partyId})

            }
        }


    })