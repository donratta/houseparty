
//setup soundcloud

angular.module('HouseParty.services.soundManager',[])
.factory('SoundManager',function($http,$location,$rootScope){

        var theList = []
        var myPlaylist;
    var currentSong = {};
    var currentPlaylist = {};
    currentPlaylist.songList = [];  //the list of the songs with the number of votes JSON object
    currentPlaylist.currentMembers=[];  //List of JSON objects with their Names
    var partyActivity = [];
    var userSetting = {};
    var isPaused = false;
    var isRepeat = false;

    return {
        getCurrentSong:function(){
            return currentSong;
        }
        ,setCurrentSong:function(song){
            currentSong = song;
        }
        ,getCurrentPlaylist:function(){
            return currentPlaylist;
        }
        ,setCurrentPlaylist:function(playlist){
            currentPlaylist = playlist;
        },
        playNewSong:function(song){

        },
        setUser:function(user){
            userSetting = user;
        },
        getUser:function(){
            return userSetting;
        },
        setPartyMembers:function(data){
            currentPlaylist.currentMembers = data;
            $rootScope.$broadcast('partyMemberChange');
        },
        getPartyMembers:function(){
            return currentPlaylist.currentMembers;
        },
        setLocalStorage:function(partyId,memberId){
            localStorage.partyId = partyId
            localStorage.memberId = memberId
        },
        searchSongsLibrary :function(songName,callback){
            $http({method: 'POST', url: '/song/search/'+songName, data:{}}).
                success(function(data, status, headers, config) {

                    callback(data,status,headers,config,false);
                }).
                error(function(data, status, headers, config) {
                    callback(data,status,headers,config,true);
                });

        },
        getPlaylistSongs : function(){
          return currentPlaylist.songList
        },
        setPlaylistSongs:function(playlistSongs){
            currentPlaylist.songList = playlistSongs;
            $rootScope.$broadcast('partyPlaylistUpdated');
        } ,
        loadPartyActivity:function(partyId,callback){
            $http.post('/houseParty/GetPartyActivityActivity',{playlistId:partyId})
                .success(function(data,status){
                  partyActivity = data;
                  callback(null,data)

                })
                .error(function(data,status){
                   toastr.error("Error Reaching Server:"+status);
                   callback(status,null);
                })
        },
        getPartyActivity:function(){
            return partyActivity;
        },

        initializePlayer:function(){
            //construct the list here
            theList = []
            for(var i = 0;i<currentPlaylist.songList.length;i++){
                var newObj = {}
                newObj.title = currentPlaylist.songList[i].song.name;
                newObj.artist= currentPlaylist.songList[i].song.artist
                newObj.mp3= currentPlaylist.songList[i].song.url
                newObj.poster =  "images/Mainlogo.png"

                theList.push(newObj);
            }
            console.log(theList)

            myPlaylist =    new jPlayerPlaylist({
                jPlayer: "#jplayer_N",
                cssSelectorAncestor: "#jp_container_N"
            },theList , {
                playlistOptions: {
                    enableRemoveControls: true,
                    autoPlay: true
                },
                swfPath: "js/jPlayer",
                supplied: "webmv, ogv, m4v, oga, mp3",
                smoothPlayBar: true,
                keyEnabled: true,
                audioFullScreen: false
            });

            $(document).on($.jPlayer.event.pause, myPlaylist.cssSelector.jPlayer,  function(){
                $('.musicbar').removeClass('animate');
                $('.jp-play-me').removeClass('active');
                $('.jp-play-me').parent('li').removeClass('active');
            });

            $(document).on($.jPlayer.event.play, myPlaylist.cssSelector.jPlayer,  function(){
                $('.musicbar').addClass('animate');
            });

            $(document).on('click', '.jp-play-me', function(e){
                e && e.preventDefault();
                var $this = $(e.target);
                if (!$this.is('a')) $this = $this.closest('a');

                $('.jp-play-me').not($this).removeClass('active');
                $('.jp-play-me').parent('li').not($this.parent('li')).removeClass('active');

                $this.toggleClass('active');
                $this.parent('li').toggleClass('active');
                if( !$this.hasClass('active') ){
                    myPlaylist.pause();
                }else{
                    var i = Math.floor(Math.random() * (1 + 7 - 1));
                    myPlaylist.play(i);
                }

            });


        },

        pushTrackForPeopleThatJustJoined:function(){
           for(var i = 0; i<currentPlaylist.length;i++){
               var newObj= {}
               newObj.title    = currentPlaylist.songList[i].song.name
               newObj.artist = currentPlaylist.songList[i].song.artist
               newObj.mp3 = currentPlaylist.songList[i].song.url
               newObj.poster =  "images/Mainlogo.png"
               myPlaylist.add(newObj);

      }


        },
        pushNewTrackToStack:function(){
             var newObj= {}
             newObj.title    = currentPlaylist.songList[currentPlaylist.songList.length-1].song.name
             newObj.artist = currentPlaylist.songList[currentPlaylist.songList.length-1].song.artist
             newObj.mp3 = currentPlaylist.songList[currentPlaylist.songList.length-1].song.url
             newObj.poster =  "images/Mainlogo.png"
             myPlaylist.add(newObj);

        }

        ,
        loadPartyMembers:function(callback){
            $http.post('/houseParty/getAllPartyMembers',{playlistId:currentPlaylist._id})
                .error(function(data,status){
                    callback(status,null)

                })
                .success(function(data,status){
                    currentPlaylist.currentMembers = data
                    callback(null, data);

                })
        }
    }

});











