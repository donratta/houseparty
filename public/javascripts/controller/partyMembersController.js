angular.module('HouseParty.controllers.partyController',[])

.controller('PartyMembersController',['$scope','SoundManager',function(scope,SoundManager){
      scope.currentPartyMembers = SoundManager.getPartyMembers();
      scope.$on('partyMemberChange',function(){
            SoundManager.loadPartyMembers(function(err,data){
                if(!err){
                    scope.currentPartyMembers = data;
                    return;
                }
                toastr.warning("Cannot get the latest party list from the server")
            })
         scope.currentPartyMembers = SoundManager.getPartyMembers();
      })
}])



.controller('createHousePartyController',['$scope','$http','$location','SoundManager',function(scope,$http,$location,SoundManager){
     scope.formData = {}
     scope.formData.name =null;
     scope.formData.nickName = null;
     scope.formData.mood = "Happy";

     scope.createParty = function(){
          $http.post('/houseParty/createHousePartyJson',scope.formData)
              .success(function(data,status){
                toastr.success('House Party Created!');
                //set the Data for the
                  SoundManager.setCurrentPlaylist(data.partyInfo);
                  SoundManager.setUser(data.myPartyInfo);
                  SoundManager.setPartyMembers(data.partyMembers);
                  //set local Storage in case page ie reloaded
                  SoundManager.setLocalStorage(data.partyInfo._id,data.myPartyInfo._id);
                //change the location to the party page
                $location.path('/myParty');
              })
              .error(function(data,status){
                toastr.error("Error:"+status);
              })
        }
    }])

.controller('JoinHousePartyController',['$scope','$http','$location','SoundManager',function(scope,$http,$location,SoundManager){
        scope.formData = {};
        scope.joinParty = function(){
            $http.post('/houseParty/joinParty',{nickName:scope.formData.nickname,partyCode:scope.formData.partyCode})
                .success(function(data,status){
                   //set the party information
                    console.log(data);

                    SoundManager.setCurrentPlaylist(data.partyInfo);
                    SoundManager.setUser(data.myPartyInfo);
                    SoundManager.setPartyMembers(data.partyMembers);
                    SoundManager.setLocalStorage(data.partyInfo._id,data.myPartyInfo._id);
                    SoundManager.setPlaylistSongs(data.playlistSongs)
                    //change the location
                    localStorage.justJoined = "true";
                    $location.path('/myParty');
                })
                .error(function(data,status){
                   toastr.error('Cannot Join The Party:'+status);
                })

        }

}])

.controller('HousePartyPageController',['$scope','SoundManager','SocketManager',function(scope,SoundManager,SocketManager){
      //get the information of the party that you have in the manager
        scope.noPartyInSession = false;
       if(localStorage.justJoined == "true"){
           SoundManager.pushTrackForPeopleThatJustJoined();
           SoundManager.initializePlayer();
           localStorage.removeItem("justJoined");
       }
       scope.partyInformation = SoundManager.getCurrentPlaylist();
        console.log(scope.partyInformation)
       if(scope.partyInformation.currentMembers.length==0){
          scope.noPartyInSession = true;
          return;
       }
       console.log(scope.partyInformation);
       scope.partySubSection = 'partyPlaylist';
       scope.searchText = {};
       scope.partyActivites = []
       scope.searchText.text = null;
       scope.changePartyTab = function(value){
          scope.partySubSection = value;
       }
        scope.reloadActivity = function(){
            SoundManager.loadPartyActivity(scope.partyInformation._id,function(err,data){
                if(err){
                    //do nothing
                }
                else{
                    scope.partyActivites = data;
                }
            })
        }
        scope.reloadActivity();

        //search track
        scope.runSearch = function(newVal){
            SoundManager.searchSongsLibrary(newVal,function(data,status,headers,config,err){
                if(err){
                    toastr.error(data.error);
                    return;
                }
                scope.songList = data;

            });

        }

        scope.addToPlaylist = function(songId){
            SocketManager.addNewSong(songId,SoundManager.getUser()._id,scope.partyInformation._id);
        }

      //  scope.$on('partyPlaylistUpdated',function(){
       //     scope.partyInformation.songList = SoundManager.getPlaylistSongs();
    //    });
    //    scope.$on('newPartyActivity',function(){
    //        scope.reloadActivity();
    //    });

}])

.controller('musicPlayerController',['$scope','$http',function(scope,$http){




    }])