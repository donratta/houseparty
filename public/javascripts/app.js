
angular.module('HouseParty', ['ui.bootstrap','ngAnimate', 'ui.router','ngRoute','HouseParty.services.soundManager','HouseParty.services.socket','HouseParty.controllers.homeController',
    'HouseParty.controllers.partyController'])

.config(['$routeProvider', '$locationProvider', function($routeProvider, $locationProvider) {

    $routeProvider.when('/home', {
            templateUrl: 'views/main.html',
            controller: 'HomeController'
        })
        .when('/createHouseParty',{
            templateUrl:"views/createHouseParty.html",
            controller: 'createHousePartyController'
        })
        .when('/myParty',{
            templateUrl:"views/houseParty.html" ,
            controller:'HousePartyPageController'
        })
        .when('/joinParty',{
            templateUrl:"views/joinParty.html",
            controller:"JoinHousePartyController"
        })
        .otherwise({
            redirectTo: '/home'
        });

    $locationProvider.html5Mode(true);

}])  ;
