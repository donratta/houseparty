var HouseParty = require('../models/houseParty') ;
var app = require('../app');
var uuid = require('node-uuid');
var express = require('express');
var router =  express.Router();
var Song = require('../models/song');
var PlaylistSong = require('../models/playlistSong') ;
var socket = require('./socket');
var HousePartyActivity = require('../models/housePartyActivity') ;
var PartyMembers = require('../models/partyMembers');
var qr = require('qr-image');






router.post('/createHousePartyJson',function(req,res){
    //get the json object from the front end and create the instance
    var newParty = req.body;
    newParty.passcode = uuid.v4();
    var splitter = newParty.passcode.split("-");
    newParty.passcode = splitter[splitter.length-1];
    HouseParty.create(newParty,function(err,party){
       if(err){
           //TODO crete a utility for sending error jSON messages back to the client
       }
        else{
           //create a member object and set him as the admin
            var newMember = new PartyMembers();
            newMember.nickName = newParty.nickName;
            newMember.admin = true;
            newMember.playlist = party._id;

            newMember.save(function(err,member){
                if(err){
                    res.json(500,err);
                    return;
                }
                //create the QR code and add it to the DB after pushing to S3
                var qr_svg = qr.image(party.passcode, { type: 'png' });
                qr_svg.pipe(require('fs').createWriteStream('public/qrimg/'+party._id+'.png'));
                //send a 200 response with the JSON thing
                //set the session of with the ID of the party for subsequent requests
                console.log(party._id)
                PartyMembers.find({playlist:party._id},function(err,members){
                    if(err){
                        res.json(500,err);
                        return;
                    }
                    var responseObj = {}
                    responseObj.partyInfo = party;
                    responseObj.myPartyInfo = member;
                    responseObj.partyMembers = members;
                    req.session.housePartyId = party._id;
                    req.session.housePartyPasscode= party.passcode;
                    res.json(200,responseObj);
                })
            })
       }
    });
})  ;


router.post('/myparty',function(req,res){
    //this will get the playlist information and and populate the songs
    if(!req.session.housePartyId){
        res.json(400,{error:"You have no party on the server"});
        return;
    }
    //
    HouseParty.findById(req.session.housePartyId,function(err,houseparty){
        if(err){
           res.json(500,err);
        return;
        }
        if(houseparty){
            //get all the songs in the party and add it to the object
            PlaylistSong.find({playlist:houseparty._id}).populate('song').exec(function(err,songs){
                if(err){
                    res.json(500,err);
                    return;
                }
                houseparty.songs = songs;
                res.json(200,houseparty);
                return;
            }) ;

        }

    });
});



router.post('/getAllPartyMembers',function(req,res){
        PartyMembers.find({playlist:req.body.playlistId},function(err,data){
            if(err){
                res.json(500,err)
                return;
            }
            res.json(200,data);

        })
})

//this will get the party for people that are trying to join
router.post('/joinParty',function(req,res){
     var partyCode = req.body.partyCode ;
    HouseParty.findOne({passcode:partyCode},function(err,party){
        if(err){
            res.json(500,err);
            return;

        }
        else if(!party){
            res.json(400,{error:"This Party has not been found"});
            return;
        }

        var newMember = new PartyMembers();
        newMember.nickName = req.body.nickName;
        newMember.playlist = party._id;
        newMember.save(function(err,newMem){
            if(err){
                res.json(500,err);
            }
            req.session.housePartyPasscode = party.passcode;
            req.session.housePartyId = party._id;
            //get the party members
            PartyMembers.find({playlist:party._id},function(err,members){
                //get the playlist list
                PlaylistSong.find({playlist:party._id}).populate('song').exec(function(err,songz){
                    if(err){
                        res.json(500,err);
                        return;
                    }

                    console.log(members) ;
                    socket.newMember(newMem);
                    socket.newMemberBroad()
                    var responseObj = {}
                    responseObj.playlistSongs = songz
                    responseObj.partyInfo = party
                    responseObj.myPartyInfo = newMem;
                    responseObj.partyMembers = members;
                    res.json(200,responseObj);

                })


            })

        })

    })
})  ;


router.post('/addSong',function(req,res){
   //check the session for the passcode
    var songId = req.body.songId;

    if(!req.session.housePartyPasscode){
        res.json(401,{error:"Join a Party first"})
        return;
    }

    //check if the song is not there

    PlaylistSong.findOne({song:songId, passcode:req.session.housePartyPasscode},function(err,song){
              if(err){
                  res.json(500,err);
                  return;
              }
            if(song){
               res.json(400,{error:"The song is already in the playlist"}) ;
                return;
            }
        HouseParty.findOne({passcode:req.session.housePartyPasscode},function(err,houseparty){
            if(err){
                res.json(500,err);
                return
            }
            if(!houseparty){
                res.json(400,{error:"party not found"});
                return;
            }
            var newEntry = {
                playlist : houseparty._id,
                song:songId,
                personAdding:null, //TODO - add session system to store the username
                passcode:houseparty.passcode
            }
        PlaylistSong.create(newEntry,function(err,entry){
            if(err){
                res.json(500,err);
            }
            Song.findById(songId,function(err,song){
                if(err){

                }
                //TODO - send socket message out to all the clients connected to this party to update their playlist
                var activity = {};
                activity.user = "nickname";
                activity.playlist = houseparty._id;
                activity.activity = "The Nickname has successfully added " + song.name + " to the houseparty playlist";
                activity.error = false;
                HousePartyActivity.create(activity,function(err,activity){
                        if(err){
                        res.json(500,err);
                        }
                    var message = {};
                    message.error = false;
                    message.text = "The Nickname has successfully added " + song.name + " by "+song.artist+" to the houseparty playlist";
                    message.housePartyPasscode = houseparty.passcode;
                    socket.sendMessage(message)  ;

                    res.json(200,entry);


                })  ;


            })

        });

        });


    });






});

router.post('/GetPartyActivityActivity',function(req,res){

    HousePartyActivity.find({playlist:req.body.playlistId},function(err,activites){
                       if(err){
                           res.json(500,err);
                           return;
                       }
        res.json(200,activites);
    });
})

router.post('/GetPartySongs',function(req,res){
      var playlist = req.body.playlistPassCode;
    PlaylistSong.find({passcode:playlist}).sort([['votes','descending']]).populate('song').exec(function(err,songs){
        if(err){
            res.json(500,err);
            return;
        }
        //console.log(songs)
        //console.log(songs)
        res.json(200,songs) ;
    }) ;


})   ;


router.post('/voteTrack',function(req,res){
    var trackId = req.body.trackId;
    var passCode = req.body.passCode;
    var name =  req.body.nickname

    PlaylistSong.findOne({_id:trackId}).populate('song').exec(function(err,song){
        if(err){
            res.json(500,{error:err});
            return;
        }
        console.log(song)
        song.votes = song.votes + 1;
        song.save(function(err){
            if(err){
                res.json(500,{error:err});
                return;
            }
            var message = {};
            message.error = false;
            message.text = "The Nickname has successfully voted " + song.song.name + " by "+song.song.artist+" to the houseparty playlist";
            message.housePartyPasscode = passCode;
            socket.sendMessage(message);
            socket.sendRefreshMessage(message)
            res.json(200,message);


        })

    })



}) ;


router.post('/getPartyMembers',function(req,res){
    PartyMembers.find({playlist:req.body.partyId},function(err,result){
        if(err){
            res.json(500,err);
            return;
        }
        res.json(200,result);
    })



})






module.exports.HousePartyrouter = router