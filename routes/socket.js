var HousePartyActivity = require('../models/housePartyActivity') ;
var PartyMembers = require('../models/partyMembers');
var PlaylistSong = require('../models/playlistSong') ;

module.exports.setup= function(server){
    var io = require('socket.io').listen(server);
    var sockets = [];
    io.on('connection',function(socket){
       // io.set("transports", ["xhr-polling"])
       // io.set("polling duration", 10);
        socket.on('addNewSong',function(data){
            //add the song to the playlist DB
                 PartyMembers.findById(data.userId,function(err,result){
                     //get the new song list
                     var playlistTrack = new PlaylistSong();
                     playlistTrack.playlist = data.partyId;
                     playlistTrack.song= data.songId;
                     playlistTrack.personAdding = result.nickName;
                     playlistTrack.save(function(err,trackAdded){
                     PlaylistSong.find({playlist:data.partyId}).populate('song')
                         .exec(function(err,results){
                         //create a new activity
                         var newActivity = new HousePartyActivity();
                             newActivity.playlist = data.partyId;
                             newActivity.activity = result.nickName+" added a track to the party!";
                             newActivity.error= false;
                             newActivity.user = result._id;
                             newActivity.save(function(err,act){
                                 if(err){
                                     console.log(err);
                                 }
                                 var newObj = {}
                                 newObj.info = result.nickName+" added a track to the party!"
                                 newObj.data = data
                                 newObj.newSongList = results;
                                 io.emit('getNewSong',newObj);

                             })


                     })
                 });
             });
        });



    });
   function sendMessage (message){
        io.emit('sendPartyActivity',message);

   }

    function sendRefreshMessage(message){
        io.emit('refreshActivity',message);
    }

    function newMember(member){
        io.emit('newPartyMember',member);
    }
    function  newMemberBroad(){
        io.emit('newMem')
    }
    module.exports.sendRefreshMessage = sendRefreshMessage
    module.exports.sendMessage = sendMessage;
    module.exports.newMember = newMember
    module.exports.newMemberBroad = newMemberBroad

     //function AL


}

