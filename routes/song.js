var HouseParty = require('../models/houseParty') ;
var app = require('../app');
var uuid = require('node-uuid');
var express = require('express');
var router =  express.Router();
var Song = require('../models/song');


router.post('/all',function(req,res){
    Song.find({},function(err,songs){
        if(err){
            res.json(500,err);
            return;
        }
        res.json(200,songs);
    });


}) ;


router.post('/search/:searchParameter',function(req,res){
    var name = req.params.searchParameter;
       Song.find({ $or:[ {'name':new RegExp(name, "i")}, {'artist':new RegExp(name, "i")} ]},function(err,songs){
               if(err){
                   res.json(500,{error:err})
                   return;
               }
               res.json(200,songs);
       });
});




module.exports.SongRouter = router