var express = require('express');
var path = require('path');
var favicon = require('static-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var  session = require('express-session');
var http = require('http');



var routes = require('./routes/index');
var users = require('./routes/users');

var app = express();
app.use(session({secret:'HFLSEE00496J4EEELLLL5500RKKRPPEMMMF'}))


// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(favicon());
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded());
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

/* Import the database */
require('./db');

/*Import the models here */

require('./models/playlistSong');
require('./models/song');
var housePartyRouter = require('./routes/houseParty').HousePartyrouter ;
var songRouter = require('./routes/song').SongRouter;

app.use('/houseparty',housePartyRouter);
app.use('/song',songRouter);


/// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function(err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}


// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
});

app.use('*',function(req,res){
    res.sendfile('./public/index.html'); //this handles the angular route
})
var server = app.listen(process.env.PORT||8080);

require('./routes/socket').setup(server);

module.exports = app;


